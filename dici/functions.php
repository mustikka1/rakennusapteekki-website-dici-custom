<?php
// MUSTIKKKA TESTING START



function wp_maintenance_mode()
{
    if (!(current_user_can('editor') || current_user_can('administrator')) || !is_user_logged_in()) {
        if (isset($_GET['pass'])) {
            return;
        }
        if (!is_page('huolto')) {
            wp_safe_redirect("/huolto/", 302, 'Maintenance');
            //wp_die('<h1>Under Maintenance</h1><br />Website under planned maintenance. Please check back later.');
            exit;
        }
        if (is_page('huolto')) {
            // TODO send 503 headers
        }
    }
}
//add_action('get_header', 'wp_maintenance_mode');

/**
 * @snippet       Custom WooCommerce Order Status
 * @how-to        Get CustomizeWoo.com FREE
 * @author        Rodolfo Melogli
 * @compatible    WooCommerce 6
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
 
add_filter( 'woocommerce_register_shop_order_post_statuses', 'register_custom_order_statuses' );
 
function register_custom_order_statuses( $order_statuses ) {
   // Status must start with "wc-"!
   $order_statuses['wc-osatoimitettu'] = array(
      'label' => 'Osatoimitettu',
      'public' => false,
      'exclude_from_search' => false,
      'show_in_admin_all_list' => true,
      'show_in_admin_status_list' => true,
      'label_count' => _n_noop( 'Osatoimitettu <span class="count">(%s)</span>', 'Osatoimitettu <span class="count">(%s)</span>', 'woocommerce' ),
   );
	
   $order_statuses['wc-odottaa-palautustuotetta'] = array(
      'label' => 'Odottaa palautustuotetta',
      'public' => false,
      'exclude_from_search' => false,
      'show_in_admin_all_list' => true,
      'show_in_admin_status_list' => true,
      'label_count' => _n_noop( 'Odottaa palautustuotetta <span class="count">(%s)</span>', 'Odottaa palautustuotetta <span class="count">(%s)</span>', 'woocommerce' ),
   );
	
	
   return $order_statuses;
}
 
add_filter( 'wc_order_statuses', 'show_custom_order_status_single_order_dropdown' );
 
function show_custom_order_status_single_order_dropdown( $order_statuses ) {
   	$order_statuses['wc-osatoimitettu'] = 'Osatoimitettu';
   	$order_statuses['wc-odottaa-palautustuotetta'] = 'Odottaa palautustuotetta';
   	$order_statuses['wc-on-hold'] = 'Ei toimitettu. Tuotteita puuttuu';
	
	// Reorder array START
	// Identify the index of 'wc-on-hold' item
	$index = array_search('wc-on-hold', array_keys($order_statuses));

	// Extract the last two items
	$lastTwo = array_slice($order_statuses, -2, 2, true);

	// Remove the last two items from the original array
	$array = array_slice($order_statuses, 0, -2, true);

	// Insert the last two items after 'wc-on-hold'
	$newArray = array_merge(
		array_slice($order_statuses, 0, $index + 1, true),
		$lastTwo,
		array_slice($order_statuses, $index + 1, null, true)
	);
	// Reorder array END
	
   	$order_statuses = $newArray;
   	return $order_statuses;
}

function lw_gpf_exclude_private_product( $excluded, $product_id, $feed_format ) {
    // return TRUE to exclude this product
    // return FALSE to include this product

	$product = wc_get_product( $product_id );

	$status = $product->get_status();
	if ($status == "publish") {
		return FALSE;
	}

    // return $excluded to keep the standard behaviour for this product.
    return TRUE; # $excluded;
}

add_filter( 'woocommerce_gpf_exclude_product', 'lw_gpf_exclude_private_product', 11, 3 );

/*
 * TODO Always translate woocommerce-delivery-notes plugin (domain) to finnish
 */
/*
// to force use English, this filter value must return true.
add_filter('override_load_textdomain', 'myPlugin_OverrideLoadTextDomain', 10, 3);

add_filter('plugin_locale', 'myPlugin_forceUseLanguageForCertainPlugin', 10, 2);

function myPlugin_OverrideLoadTextDomain($override, $domain, $mofile)
{
    if ($domain === 'woocommerce') // change text domain from woocommerce to what you want.
    {
        $override = true;
    }

    return $override;
}

function myPlugin_forceUseLanguageForCertainPlugin($locale, $domain)
{
    if ($domain === 'woocommerce') // change text domain from woocommerce to what you want.
    {
        $locale = 'de_DE';// change your locale here to whatever you want.
    }

    return $locale;
}

*/

/*
 * Disable products with custom field "not_purchasable" from being purchased no matter what
 */

add_filter('woocommerce_adjust_non_base_location_prices', '__return_false');
add_filter('woocommerce_order_hide_zero_taxes', '__return_false');
function misha_catalog_mode_on_for_category($is_purchasable, $product)
{
    // Get the product attribute value(s)
    $not_purchasable = get_post_meta($product->get_id(), 'not_purchasable', true);

    if ($not_purchasable) {
        return false;
    }

    return $is_purchasable;
}


add_filter('woocommerce_is_purchasable', 'misha_catalog_mode_on_for_category', 10, 2);

/**
 * Set a minimum order amount for checkout
 */
//add_action('woocommerce_checkout_process', 'wc_minimum_order_amount');
//add_action('woocommerce_before_cart', 'wc_minimum_order_amount');

function wc_minimum_order_amount()
{
    // Set this variable to specify a minimum order value
    $minimum = 1;

    if (WC()->cart->total < $minimum && WC()->cart->total > 0) {

        if (is_cart()) {

            wc_print_notice(
                sprintf(
                    'Tilauksesi kokonaissumma on %s — Tilauksen minimi summa on oltava vähintään %s',
                    wc_price(WC()->cart->total),
                    wc_price($minimum)
                ),
                'error'
            );
        } else {

            wc_add_notice(
                sprintf(
                    'Tilauksesi kokonaissumma on %s — Tilauksen minimi summa on oltava vähintään %s',
                    wc_price(WC()->cart->total),
                    wc_price($minimum)
                ),
                'error'
            );
        }
    }
}



// MUSTIKKA TESTING END


/**
 * dici functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package dici
 */

if ( !defined( 'THEMESZONE_THEME_VERSION' ) ) define('THEMESZONE_THEME_VERSION', '1.0.0');

if ( ! function_exists( 'dici_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function dici_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on dici, use a find and replace
		 * to change 'dici' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'dici', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );


		register_nav_menus( array(
			'menu-main' => esc_html__( 'Primary', 'dici' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'dici_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
			'header-text' => array( 'site-title', 'site-description' ),
		) );

		/**
		 * Add support for all post formats.
		 *
		 * @link https://codex.wordpress.org/Post_Formats
		 */
		add_theme_support( 'post-formats', array(
			'aside',
			'gallery',
			'link',
			'image',
			'quote',
			'status',
			'video',
			'audio',
			'chat'
		) );

		/**
		 * Add support for Yoast SEO plugin breadcrumbs
		 *
		 * @link https://kb.yoast.com/kb/add-theme-support-for-yoast-seo-breadcrumbs/
		 */
		add_theme_support( 'yoast-seo-breadcrumbs' );
        add_theme_support( 'editor-styles' );
        add_theme_support( 'disable-custom-colors' );
        add_editor_style( 'style-editor.css' );

        add_theme_support( 'editor-color-palette', array(
            array(
                'name'  => esc_html__( 'DiCi Accent Color', 'dici' ),
                'slug'  => 'dici-accent-color',
                'color'	=> '#00d1b7',
            ),
            array(
                'name'  => esc_html__( 'Dici Accent Text Color', 'dici' ),
                'slug'  => 'dici-accent-text-color',
                'color' => '#109d92',
            ),
            array(
                'name'  => esc_html__( 'DiCi Text Color', 'dici' ),
                'slug'  => 'dici-main-text-color',
                'color' => '#262626',
            ),
            array(
                'name'  => esc_html__( 'DiCi Secondary Deco Color', 'dici' ),
                'slug'  => 'dici-second-deco-color',
                'color' => '#939393',
            ),
            array(
                'name'  => esc_html__( 'DiCi Hover Color', 'dici' ),
                'slug'  => 'dici-hover-color',
                'color' => '#363636',
            ),
        ) );

        add_theme_support( 'editor-font-sizes', array(
            array(
                'name'      => esc_html__( 'Extra Small', 'dici' ),
                'shortName' => esc_html__( 'XS', 'dici' ),
                'size'      => 11,
                'slug'      => 'xsmall'
            ),
            array(
                'name'      => esc_html__( 'Small', 'dici' ),
                'shortName' => esc_html__( 'S', 'dici' ),
                'size'      => 14,
                'slug'      => 'small'
            ),
            array(
                'name'      => esc_html__( 'Normal', 'dici' ),
                'shortName' => esc_html__( 'M', 'dici' ),
                'size'      => 15,
                'slug'      => 'normal'
            ),
            array(
                'name'      => esc_html__( 'Large', 'dici' ),
                'shortName' => esc_html__( 'L', 'dici' ),
                'size'      => 20,
                'slug'      => 'large'
            ),
            array(
                'name'      => esc_html__( 'Larger', 'dici' ),
                'shortName' => esc_html__( 'XL', 'dici' ),
                'size'      => 32,
                'slug'      => 'larger'
            ),
            array(
                'name'      => esc_html__( 'Largest', 'dici' ),
                'shortName' => esc_html__( 'XXL', 'dici' ),
                'size'      => 40,
                'slug'      => 'largest'
            )
        ) );


	}
endif;
add_action( 'after_setup_theme', 'dici_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function dici_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'dici_content_width', 1380 );
}
add_action( 'after_setup_theme', 'dici_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function dici_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Blog Sidebar', 'dici' ),
		'id'            => 'sidebar-blog', // Do not modify ids
		'description'   => esc_html__( 'Add widgets here.', 'dici' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Page Sidebar', 'dici' ),
		'id'            => 'sidebar-page', // Do not modify ids
		'description'   => esc_html__( 'Add widgets here.', 'dici' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'dici_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function dici_scripts() {

	wp_enqueue_style( 'dici', get_stylesheet_uri() );

	wp_enqueue_script( 'dici-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'dici-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_style( 'dici-custom-fonts', get_template_directory_uri().'/assets/css/fonts.css');

	wp_enqueue_style( 'dici-icons',  get_template_directory_uri().'/assets/css/dici-icons.css');

	wp_enqueue_script( 'dici', get_template_directory_uri() . '/js/main.js', array(), THEMESZONE_THEME_VERSION, true );

	wp_localize_script( 'dici', 'theme_vars', array('theme_prefix' => 'dici', 'js_path' => get_template_directory_uri() . '/js/') );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'dici_scripts' );

function dici_get_js_theme_vars(){

}

add_filter('smartslider3_skip_license_modal', 'ellie_smartslider3_skip_license_modal' );
function ellie_smartslider3_skip_license_modal() {
	return true;
}

/**
 * Load Theme Helper
 */
require get_template_directory() . '/inc/lib/class-tz-theme-helper.php';

/**
 * Load Theme Controller
 */
require get_template_directory() . '/inc/lib/class-tz-theme-controller.php';
require get_template_directory() . '/inc/class-dici-controller.php';
// Run controller
add_action( 'after_setup_theme', 'DiCi_Controller::instance' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom header tags for this theme.
 */
require get_template_directory() . '/inc/header-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * Load required plugins prompt.
 */
require get_template_directory() . '/inc/required-plugins.php';

/**
 * Load demo data prompt.
 */
if ( class_exists( 'OCDI_Plugin' ) ) {
	require get_template_directory() . '/inc/demo-import.php';
}
add_filter('the_content', 'customizing_woocommerce_description');
function customizing_woocommerce_description($content)
{
    $custom_content = '';
    if (is_product()) {
        global $post;
        $product_id = $post->ID;
        $ex_return = get_field("exchange_and_refund", $product_id);
        if ($ex_return == 'no') {
            $field_txt = get_field("non_exchange_text", $product_id);
            if (empty($field_txt)) {
                $field_txt = acf_get_field('non_exchange_text')['default_value'];
            }
            $custom_content = '<div class="non-exchange-txt">' . $field_txt . '</div>';
        }

        $content .= $custom_content;
    }
    return $content;
}
add_filter('woocommerce_product_tabs', 'force_description_product_tabs');
function force_description_product_tabs($tabs)
{
    if (is_product()) {
        global $post;
        $product_id = $post->ID;
        $ex_return = get_field("exchange_and_refund", $product_id);
        if ($ex_return == 'no') {

            $tabs['description'] = array(
                'title'    => __('Description', 'woocommerce'),
                'priority' => 10,
                'callback' => 'woocommerce_product_description_tab',
            );
        }
    }

    return $tabs;
}
add_action('woocommerce_product_meta_end', 'display_downloadable_pdf');
function display_downloadable_pdf()
{
    global $post;
    $product_id = $post->ID;
    $file = get_field("pdf_file", $product_id);
    if ($file) { ?>
        OHJEITA JA MUITA TIEDOSTOJA: <a class="product-pdf-download" href="<?php echo $file['url']; ?>"><i class="fa fa-file-pdf" aria-hidden="true"></i> <?php echo $file['filename']; ?></a>
    <?php }
}

add_action('woocommerce_review_order_before_payment', 'wc_privacy_message_below_checkout_button');

function wc_privacy_message_below_checkout_button()
{
    echo '<style>.payment_method_heading p {
    margin-top: 10px;
    padding: 1em;
    background-color: #f8f9fa;
}</style><div class="payment_method_heading"><p>Valitse maksutapa</p></div>';
}
add_filter('default_checkout_billing_country', 'change_default_checkout_country');

function change_default_checkout_country()
{
    return 'FI';
}


add_filter('woocommerce_redirect_single_search_result', '__return_false');

add_filter('woocommerce_product_categories_widget_args', 'bbloomer_hide_current_cat_prod_cat_widget');
add_filter('woocommerce_product_categories_widget_dropdown_args', 'bbloomer_hide_current_cat_prod_cat_widget');

function bbloomer_hide_current_cat_prod_cat_widget($args)
{

    $current_cat_id = '1155';
    $args['exclude'] = $current_cat_id;

    return $args;
}
function privateposts_in_search($query)
{
    global $loggedInUserCheck;


    // START MUSTIKKA FIX
    $post_type = $query->get('post_type');
    if (is_string($post_type)) {
        $post_type  = ($post_type === "product");
    } else {
        $post_type  = in_array("product", $post_type);
    }

    // if(!is_admin() && in_array("product", $query->get('post_type')));
    if (!is_admin() && $post_type) {
        if ($loggedInUserCheck) {
            $query->set('post_status', array('private', 'publish'));
        }
    }

    // END MUSTIKKA FIX

}
add_action('pre_get_posts', 'privateposts_in_search');
add_action('init', 'setglobalvar');

function setglobalvar()
{
    global $loggedInUserCheck;
    $loggedInUserCheck = is_user_logged_in();
}

add_action('woocommerce_shop_loop_item_title', 'prima_custom_shop_item', 5);
function prima_custom_shop_item()
{
    global $post, $product;
    echo wc_get_stock_html($product);
    if ($product->get_sku()) {
        echo '<span class="category-sku-display">SKU: ' . $product->get_sku() . '</span>';
    }
}
add_action('woocommerce_before_shop_loop', 'woocommerce_catalog_page_filterScroll', 20);
function woocommerce_catalog_page_filterScroll()
{
    echo '<div class="woocommerce-ordering wlk_filter_scroll"><a class="select2 select2-container select2-container--default" href="#woocommerce_layered_nav-3"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-orderby-59-container"><span class="select2-selection__rendered" id="select2-orderby-59-container" role="textbox" aria-readonly="true" title="' . __('Suodattimet', 'woocommerce') . '">' . __('Suodattimet', 'woocommerce') . '</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span></a></div>';
}
add_action('woocommerce_before_shop_loop', 'woocommerce_catalog_page_ordering', 20);

function woocommerce_catalog_page_ordering()
{
    $current_url =  $_SERVER['REQUEST_URI'];
    $pattern = '/page\\/[0-9]+\\//i';
    $nopaging_url = preg_replace($pattern, '', $current_url);
    $per_page = filter_input(INPUT_GET, 'perpage', FILTER_SANITIZE_NUMBER_INT);
    echo '<style>.woocommerce-ordering span.select2.select2-container.select2-container--default {
    width: auto !important;
}</style>';
    echo '<div class="woocommerce-ordering perpage_display"><span class="select2 select2-container select2-container--default">Näytä</span>';
    echo '<form id="perpage_form" style="display:inline-block" method="get" action="' . site_url($nopaging_url) . '" >';
    echo '<select id="perpage_select" name="perpage" onchange="this.form.submit()">';
    $orderby_options = array(
        '12' => '12',
        '24' => '24',
        '32' => '32',
        '64' => '64',
        //'-1' => __('All', 'woocommerce'),
    );
    foreach ($orderby_options as $value => $label) { ?>
        <option value="<?php echo esc_attr($value); ?>" <?php selected($per_page, $value); ?>><?php echo esc_html($label); ?></option>
<?php }
    echo '</select>';
    //echo '<input type="hidden" name="paged" value="1" />';
    wc_query_string_form_fields(null, array('perpage', 'submit', 'paged', 'product-page'));
    echo '</form>';
    echo '</div>';
}

function dici_woocommerce_products_per_page_custom()
{
    $per_page = filter_input(INPUT_GET, 'perpage', FILTER_SANITIZE_NUMBER_INT);
    return ($per_page) ? $per_page : 12;
}
add_filter('loop_shop_per_page', 'dici_woocommerce_products_per_page_custom');
add_filter('woocommerce_get_breadcrumb', 'tm_child_remove_product_title', 10, 2);

function tm_child_remove_product_title($crumbs, $breadcrumb)
{
    if (is_product()) {
        array_pop($crumbs);
    }
    return $crumbs;
}

function search_by_sku($search, &$query_vars)
{
    global $wpdb;
    if (isset($query_vars->query['s']) && !empty($query_vars->query['s'])) {
        $args = array(
            'posts_per_page'  => -1,
            'post_type'       => 'product',
            'meta_query' => array(
                'relation' => 'OR',
                array(
                    'key' => '_sku',
                    'value' => $query_vars->query['s'],
                    'compare' => 'LIKE'
                ),
                array(
                    'key' => 'EAN',
                    'value' => $query_vars->query['s'],
                    'compare' => 'LIKE'
                )
            )
        );
        $posts = get_posts($args);
        if (empty($posts)) return $search;
        $get_post_ids = array();
        foreach ($posts as $post) {
            $get_post_ids[] = $post->ID;
        }
        if (sizeof($get_post_ids) > 0) {
            $search = str_replace('AND (((', "AND ((({$wpdb->posts}.ID IN (" . implode(',', $get_post_ids) . ")) OR (", $search);
        }
    }
    return $search;
}
//add_filter( 'posts_search', 'search_by_sku', 999, 2 );

add_filter('posts_search', 'woocommerce_search_product_mega_extended', 999, 2);
function woocommerce_search_product_mega_extended($search, $query)
{
    global $wpdb, $wp;

    $qvars = $wp->query_vars;

    if (is_admin() || empty($search) ||  !(isset($qvars['s'])
        && isset($qvars['post_type']) && !empty($qvars['s'])
        && $qvars['post_type'] === 'product')) {
        return $search;
    }

    // SETTINGS:
    $taxonomies = array('product_tag', 'product_cat'); // Here set your custom taxonomies in the array
    $meta_keys  = array('_sku', 'EAN', '_search_words_se', '_search_words_en', '_search_words_de', '_search_words_fi'); // Here set your product meta key(s) in the array

    // Initializing tax query
    $tax_query  = count($taxonomies) > 1 ? array('relation' => 'OR') : array();

    // Loop through taxonomies to set the tax query
    foreach ($taxonomies as $taxonomy) {
        $tax_query[] = array(
            'taxonomy' => $taxonomy,
            'field'    => 'name',
            'terms'    => esc_attr($qvars['s']),
        );
    }

    // Get the product Ids from taxonomy(ies)
    $tax_query_ids = (array) get_posts(array(
        'posts_per_page'  => -1,
        'post_type'       => 'product',
        'post_status'     => 'publish',
        'fields'          => 'ids',
        'tax_query'       => $tax_query,
    ));

    // Initializing meta query
    $meta_query = count($meta_keys) > 1 ? array('relation' => 'OR') : array();

    // Loop through taxonomies to set the tax query
    foreach ($meta_keys as $keys) {
        $meta_query[] = array(
            'key' => $keys,
            'value'   => esc_attr($qvars['s']),
            'compare' => 'LIKE'
        );
    }

    // Get the product Ids from custom field(s)
    $meta_query_ids = (array) get_posts(array(
        'posts_per_page'  => -1,
        'post_type'       => 'product',
        'post_status'     => 'publish',
        'fields'          => 'ids',
        'meta_query'      => $meta_query,
    ));

    $product_ids = array_unique(array_merge($tax_query_ids, $meta_query_ids)); // Merge Ids in one array  with unique Ids

    if (sizeof($product_ids) > 0) {
        $search = str_replace('AND (((', "AND ( ({$wpdb->posts}.ID IN (" . implode(',', $product_ids) . ")) OR ((", $search);
        // $search .= " OR ($wpdb->posts.id  IN (" . implode(',', $product_ids) . "))";
    }

    return $search;
}


add_filter('wp_mail_from', 'sender_email');
function sender_email($original_email_address)
{
    return 'noreply@rakennusapteekki.fi';
}
add_filter('wp_mail_from_name', 'sender_name');
function sender_name($original_email_from)
{
    return 'Rakennusapteekki - Byggnadsapoteket';
}
add_action('woocommerce_after_add_to_cart_button', 'add_content_after_addtocart_button_func', -1);
function add_content_after_addtocart_button_func()
{
    global $product;
    $pro_ids = $product->get_cross_sell_ids();
    if ($pro_ids) :
        echo '<div class="cross_sell_item_wrap">';
        $cross_sell_items = array();
        foreach ($pro_ids as $pro_id) {
            $crossSellProduct = wc_get_product($pro_id);
            if (is_object($crossSellProduct) && $crossSellProduct->get_attribute('pa_cross-sell-otsikko')) {
				$crossItemVal = trim($crossSellProduct->get_attribute('pa_cross-sell-otsikko'));
                $cross_sell_items[] = '<div><a class="custom-cross-sell-btn" href="' . esc_url(get_permalink($pro_id)) . '">' .$crossItemVal. '</a></div>';
            }
        }
        echo implode(" ", $cross_sell_items);
        echo '</div>';
    endif;
}
