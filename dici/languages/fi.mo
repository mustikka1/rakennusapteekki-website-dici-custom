��                �      �     �     �     �               #     :  \   H     �     �  '   �     �     �               %     =  U   R  ;   �  c   �     H  	   T     ^     j  	   |     �     �     �  �  �     [     m     s          �     �     �  ^   �            -   <     j     q     �     �     �     �  S   �  ;   0	  _   l	     �	     �	  	   �	     �	     �	     
     
     '
   Add a review Blog Continue Reading Name Nothing Found Search Results for: %s Shopping Cart Sorry, but nothing matched your search terms. Please try again with some different keywords. Type Updating Plugin: %s Upgrade message from the plugin author: Version View your shopping cart Website Widget Area After Logo Widget Area Before Logo WordPress Repository You can always restore default values for PHP variables after installing sample data. You can find Kirki plugin in the WordPress plugin directory You have installed Kirki. Activate it to take advantage of this theme's features in the customizer. Your E-Mail Your Name Your Review labelSearch for: next post placeholderType to search previous post submit buttonSearch Project-Id-Version: DiCi
POT-Creation-Date: 2019-05-23 15:42+0100
PO-Revision-Date: 2022-04-11 12:54+0000
Last-Translator: Rudolf Ringbom
Language-Team: Suomi
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SearchPath-0: .
Report-Msgid-Bugs-To: 
X-Loco-Version: 2.6.1; wp-5.9.3 Lisää arvostelu Blogi Lue lisää Nimi Mitään ei löytynyt Hakutulokset: %s Ostoskärry Valitettavasti mikään ei vastannut hakutermejäsi. Yritä uudelleen eräillä avainsanoilla. Tyyppi Päivitetään laajennusta: %s Päivitä viesti laajennuksen kirjoittajalta: Versio Tarkastele ostoskoriasi Verkkosivusto Widget-alue logon jälkeen Widget-alue ennen logoa WordPress-arkisto Voit aina palauttaa PHP-muuttujien oletusarvot näytetietojen asentamisen jälkeen. Kirki-laajennuksen löydät WordPress-laajennushakemistosta Olet asentanut Kirkin. Aktivoi se hyödyntääksesi tämän teeman ominaisuuksia muokkauksessa. Sähköposti Nimi Arvostelu Etsiä: seuraava postaus Kirjoita hakeaksesi edellinen postaus Hae 