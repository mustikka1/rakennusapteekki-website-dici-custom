��          �       �      �     �     �     �     �     �       \        p     u  '   �     �     �     �     �     �       U     ;   s  c   �       	        )     5     G     b  �  w     
          #     (     >     U  ^   a     �     �  -   �               3     A     \     t  S   �  ;   �  _   	     v	     �	  	   �	     �	     �	     �	   Add a review Continue Reading Name Nothing Found Search Results for: %s Shopping Cart Sorry, but nothing matched your search terms. Please try again with some different keywords. Type Updating Plugin: %s Upgrade message from the plugin author: Version View your shopping cart Website Widget Area After Logo Widget Area Before Logo WordPress Repository You can always restore default values for PHP variables after installing sample data. You can find Kirki plugin in the WordPress plugin directory You have installed Kirki. Activate it to take advantage of this theme's features in the customizer. Your E-Mail Your Name Your Review labelSearch for: placeholderType to search submit buttonSearch Project-Id-Version: DiCi
POT-Creation-Date: 2019-05-23 15:42+0100
PO-Revision-Date: 2021-10-17 03:17+0000
Last-Translator: Raksha D
Language-Team: English
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SearchPath-0: .
Report-Msgid-Bugs-To: 
X-Loco-Version: 2.5.4; wp-5.8.1 Add a review Lue lisää Nimi Mitään ei löytynyt Search Results for: %s Ostoskärry Valitettavasti mikään ei vastannut hakutermejäsi. Yritä uudelleen eräillä avainsanoilla. Tyyppi Päivitetään laajennusta: %s Päivitä viesti laajennuksen kirjoittajalta: Versio Tarkastele ostoskoriasi Verkkosivusto Widget-alue logon jälkeen Widget-alue ennen logoa WordPress-arkisto Voit aina palauttaa PHP-muuttujien oletusarvot näytetietojen asentamisen jälkeen. Kirki-laajennuksen löydät WordPress-laajennushakemistosta Olet asentanut Kirkin. Aktivoi se hyödyntääksesi tämän teeman ominaisuuksia muokkauksessa. Sähköposti Nimi Arvostelu Etsiä: Kirjoita hakeaksesi Hae 