( function( $ ) {
    "use strict";

    function useSelect2(){
        var selects = $('' +
            '.buttons-wrapper .variations select,' +
            '.woocommerce-ordering select,' +
            '.dropdown_product_cat,' +
            '.variations select,' +
            '.tinvwl-break-input-filed,' +
            '.widget_archive select,' +
            '.widget_categories select,' +
            '#secondary .widget select');
        if ( selects.length && ( typeof $(document).select2 == 'function' ) ) {
            $(selects).select2({
                minimumResultsForSearch: 40
            });
        }
    }

    useSelect2();

	$('.search-form').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) {
    e.preventDefault();
   location.href = $(this).attr('action')+'?'+$(this).serialize();
  }
});
$('#fpde-popup-wrapper').bind('DOMSubtreeModified', function(){
$('input[name="fpde-reg-price"],input[name="fpde-sale-price"]').attr('step','any');
});


$('.wlk_filter_scroll a').click(function(e){
e.preventDefault();
let scrollSectionId = $( $("aside#secondary section")[1] ).attr('id');
$('html,body').animate({scrollTop: $('#'+scrollSectionId).offset().top},'slow');

});

} )( jQuery );