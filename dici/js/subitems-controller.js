jQuery(document).ready(function($) {
    "use strict";

    var menuToggle = function(togglers){
        if ( togglers.length ) {

            $(togglers).each( function(){
                $(this).on('click', function(){

                    var container = $(this).parent();
                    
                    if ( $(container).hasClass('expanded') ) {
                        var MelementToExpand = $(container).find('ul.children:first, ul.sub-menu:first');
                        $(MelementToExpand).removeAttr('style');
                        $(container).toggleClass('expanded');
                        if ( $(container).parent().hasClass('children') ) {

                            var elementToClose = $(container).find('ul.children:first, ul.sub-menu:first');
                            var elementToCloseHeight = 0;
                            $(elementToClose).each(function(){
                                elementToCloseHeight += $(this).get(0).scrollHeight;
                            });
                                
                            let grandParentHeight = parseInt($(container).parent().attr('style').match(/\d+/));
                            let  grandParentMaxHeight= grandParentHeight-elementToCloseHeight
                            
                            $(container).parent().attr('style', 'max-height:'+grandParentMaxHeight+'px;');
                        }
                    } else {
                        $(container).toggleClass('expanded');
                        var elementToExpand = $(container).find('ul.children:first, ul.sub-menu:first');
                        var curElementToExpand = $(container).find('ul.children:first, ul.sub-menu:first');
                        var elementToExpandHeight = 0;
                        $(elementToExpand).each(function(){
                            elementToExpandHeight += $(this).get(0).scrollHeight;
                        });
                        $(curElementToExpand).attr('style', 'max-height:'+elementToExpandHeight+'px;');
                        if ( $(container).parent().hasClass('children') ) {
                            let grandParentHeight = parseInt($(container).parent().attr('style').match(/\d+/));
                            let  grandParentMaxHeight= grandParentHeight+elementToExpandHeight
                            
                            $(container).parent().attr('style', 'max-height:'+grandParentMaxHeight+'px;');
                        }
                    }
                });
            } );
        }
    }

    var subMenuToggler = function(){
        var parent_items = $('.widget_nav_menu li[class*=has-children], .widget_product_categories li[class*=parent]');
        if (parent_items.length){
            $(parent_items).append('<span class="expand"></span>');
        }

        var togglers = $('.widget_nav_menu li[class*=has-children] > span.expand, .widget_product_categories li[class*=parent] > span.expand');

        menuToggle(togglers);

    };

    subMenuToggler();

    var mobileMainMenuController = function(mediaQuery){
        if (mediaQuery.matches) { // If media query matches
            var parent_items = $('#site-navigation > ul > li[class*=has-children]');
            if ( parent_items.length ) {
                $(parent_items).append('<span class="expand"></span>');
                var togglers = $('#site-navigation > ul > li[class*=has-children] > span.expand');
                menuToggle(togglers);
            }

        } else {
            var toggleItems = $('#site-navigation > ul > li[class*=has-children] > span.expand');
            toggleItems.parent().removeClass('expanded');
            toggleItems.find('ul.sub-menu').removeAttr('style');
            toggleItems.remove();
        }
    };

    var mediaQuery = window.matchMedia("(max-width: 740px)");
    mobileMainMenuController(mediaQuery); // Call listener function at run time
    mediaQuery.addListener(mobileMainMenuController); // Attach listener function on state changes

var container = $(".current-cat.cat-parent , .cat-parent.current-cat-parent");
console.log("container with child--",container);
                    if ( $(container).hasClass('expanded') ) {
                        var MelementToExpand = $(container).find('ul.children:first, ul.sub-menu:first');
                        $(MelementToExpand).removeAttr('style');
                        $(container).toggleClass('expanded');
                    } else {
                        $(container).toggleClass('expanded');
                        var elementToExpand = $(container).find('ul.children:first, ul.sub-menu:first');
                        var curElementToExpand = $(container).find('ul.children:first, ul.sub-menu:first');
                        var elementToExpandHeight = 0;
                        $(elementToExpand).each(function(){
                            elementToExpandHeight += $(this).get(0).scrollHeight;
                        });
                        $(curElementToExpand).attr('style', 'max-height:'+elementToExpandHeight+'px;');
                    }

});